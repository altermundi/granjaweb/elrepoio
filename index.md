---
title: elRepo.io
subtitle: ¡aterrizando la nube!
layout: page
hero_height: is-fullwidth is-fullheight
hero_image: /img/hero.elrepoio.png
hero_container: has-text-centered
hero_link_text: código
hero_link: http://gitlab.com/elRepo.io
---

# ¿Cuál sería el sistema óptimo para compartir la cultura?
Entendemos que los sistemas existentes son sub-óptimos, no por barreras tecnológicas, sino principalmente por poderosos intereses que crean una escasez artificial que permite la monetización de los “bienes culturales”. Toda la cultura digitalizable podría estar ahora al alcance de todos, en cualquier lugar, en todo momento, con un coste marginal cero. Al mismo tiempo, las decisiones sobre qué contenidos son relevantes (e incluso localizables) en los silos de información de la corriente dominante son una forma de control social y de orientación de la opinión pública sobre la que las comunidades no tienen voz ni voto.

El impacto en la humanidad de permitir que los intereses basados en el lucro y la concentración de intereses sean los que orienten cómo evoluciona la cultura humana limitando qué información es la que podemos acceder y compartir, cuándo y dónde, es un riesgo demasiado grande para seguir asumiéndolo. Casi cualquier otro gran problema en nuestra sociedad global está directamente relacionado con éste.

# Estamos desarrollando un repositorio descentralizado de cultura….

que permite publicar y compartir contenidos de forma organizada, adecuados para audio, vídeo, texto y otros formatos. El sistema se basa en tecnologías consolidadas que aprovechan las capacidades de las Redes Comunitarias distribuidas. Esto permitirá a las comunidades compartir la cultura de manera duradera, priorizando el intercambio local sin perder la capacidad de llegar a una audiencia global.

# Organizado, resistente y con perspectiva local
## Las comunidades podrán:
- Reducir las necesidades de ancho de banda de Internet mediante el intercambio de información a nivel local incluso cuando la conectividad a Internet no funciona.
- Conservar, organizar y clasificar el contenido manteniendo ambos datos y metadatos descentralizados.
- Almacenar el contenido de forma local y fiable, reducir el riesgo de ser censurado o derribado.

# ¿En qué estamos trabajando?

Este proyecto ha sido financiado inicialmente por la Asociación para el Progreso de las Comunicaciones (APC), de la cual AlterMundi es una organización miembro. El primer ciclo de desarrollo finalizó el 31 de diciembre de 2018. El trabajo realizado durante este año nos ha ayudado a identificar un adecuado stack de tecnologías para utilizar como base para este proyecto, trabajar en la adaptación de las mismas a nuestras necesidades y desarrollar un prototipo inicial.

Hemos trabajado en libretroshare (el componente base del stack) en múltiples áreas tales como: mejorar el soporte para Android; crear un servicio multiplataforma y distribuirlo como un paquete independiente en el que muchas aplicaciones pueden confiar; programar el generador automático JSON API libretroshare para exponer todo el potencial de libretroshare a los desarrolladores de aplicaciones móviles y basadas en la web; mejorar el soporte para IPv6; implementar la indexación de metadatos a través de libxapian y utilizarlo para dar mejores resultados en la búsqueda de contenido distribuido y metadatos.

Todas nuestras modificaciones a libretroshare han sido bien recibidas y formarán parte de la próxima versión oficial de libretroshare v0.6.5. También hemos establecido una infraestructura redundante de primer nivel que ayuda a crear una experiencia de usuario sin fisuras para los recién llegados y facilita el aprendizaje progresivo de cómo se comporta una red F2F. La infraestructura Tier1 es importante especialmente al principio, cuando aún no se ha formado una masa crítica; una vez que los usuarios se unen de nuevo y la red empieza a parecerse a un mundo pequeño, el efecto de red se desencadena y la infraestructura Tier1 pierde gradualmente su importancia.

También estamos desarrollando una interfaz de usuario móvil y de escritorio que permitirá a las comunidades comenzar a utilizar el sistema e informar de sus comentarios al proceso de desarrollo iterativo. Los clientes auto-descubren a sus vecinos en su dominio de transmisión local, facilitando la comunicación dentro de una red comunitaria u otras redes que asignan su stack de redes a los vínculos sociales existentes. Esto, junto con las capacidades offline de libretroshare, permite a los usuarios descubrir nuevos amigos, interactuar y compartir contenidos sin necesidad de tener acceso a Internet. Todo lo anterior serán los resultados de nuestro primer ciclo de desarrollo.

![](/img/elrepoio_capturas_web2.png)

# Lo que queremos hacer ahora

A partir de nuestro trabajo con las Redes Comunitarias del Sur Global, hemos identificado que uno de los principales impedimentos para que un sistema descentralizado de intercambio de cultura prospere es la baja disponibilidad de dispositivos capaces de proporcionar suficientes recursos (almacenamiento, ancho de banda, tiempo en línea). Sin embargo, es muy común encontrar un televisor en cada hogar. Por lo tanto, nuestro objetivo es producir una versión de elRepo.io que se instalará en pequeños dispositivos empotrados (por ejemplo, RaspberryPi) que pueden conectarse directamente a un televisor y conectarse a la red. Estos “dispositivos base” servirán como una nube local para los miembros del hogar, permitiendo que sus aplicaciones móviles elRepo.io se conecten como un control remoto para compartir y descargar contenido. Los dispositivos base servirán como nodos siempre activos en la red distribuida, reduciendo así la dependencia de los teléfonos móviles de gama baja, con su limitado almacenamiento, ancho de banda y duración de la batería.

También hemos identificado un número importante de comunidades que están trabajando en la creación de infraestructura de comunicaciones locales pero que no tienen planes de interconectarse a la Internet global en un futuro cercano. Estas redes se beneficiarían enormemente de la posibilidad de implementar estrategias “sneakernet” a través de elRepo.io. Las radios comunitarias locales que deseen compartir sus podcasts, los medios digitales locales que deseen llegar a una audiencia global, entre otras experiencias podrían fácilmente producir su contenido localmente y entregarlo a través de un “courrier” elRepo.io a una ubicación conectada donde pueda ser compartido con la red global.

<section class="section">
  <div class="container">
	   <div class="columns is-multiline">
		<div class="column is-one-quater"><img src="/img/cropped-portada.png" alt=""></div>
		<div class="column is-one-quater"><img src="/img/cropped-portada-1-1.png" alt=""></div>
		<div class="column is-one-quater"><img src="/img/cropped-portada-1-2.png" alt=""></div>
		<div class="column is-one-quater"><img src="/img/cropped-portada-1-3.png" alt=""></div>
	</div>
  </div>
</section>

# ¿Quienes somos?
AlterMundi, junto con organizaciones asociadas de todos los continentes, ha desarrollado a lo largo de los años un stack de tecnología para las comunidades que desean crear su propias infraestructuras de comunicaciones. 
{: .blue}

Creemos que el territorio que hemos cuidado a lo largo de los años construyendo infraestructuras tecnológicas debe servir como un terreno propicio para un ecosistema de servicios y aplicaciones que ponga a la comunidad en el centro.
{: .alert .alert-info .text-center}

elRepo.io, nuestro repositorio descentralizado de cultura, puede convertirse en un proyecto fundamental en este ecosistema para que las redes comunitarias creen valor local centrado en el ser humano.

<section class="section">
  <div class="container">
	   <div class="columns is-multiline">
		<div class="column is-one-third"> {% include card.html card_link="https://altermundi.net" card_image="/img/AlterMundi.png" card_title="AlterMundi" card_subtitle="... la pata tecnológica de ese Otro Mundo posible" card_content="AlterMundi fomenta el surgimiento de un nuevo paradigma basado en la libertad obtenida a través de la colaboración entre pares." %} </div>
        <div class="column is-one-third"> {% include card.html card_link="https://libremesh.org" card_image="/img/libremesh.png" card_title="LibreMesh" card_content="Es un framework modular para crear firmwares basados en OpenWrt/LEDE para nodos mesh inalámbricos. Varias comunidades alrededor del mundo usan LibreMesh como la base de sus firmwares mesh locales." %} </div>
        <div class="column is-one-third"> {% include card.html card_link="https://librerouter.org" card_image="/img/librerouter.png" card_title="LibreRouter" card_content="El proyecto LibreRouter ha diseñado y producido un router inalámbrico multirradio de alto rendimiento dirigido a las necesidades de las redes comunitarias. La realidad del Sur Global y de América Latina en particular ha sido especialmente considerada en términos de costo y viabilidad legal." %} </div>
	</div>
  </div>
</section>
