---
en: true
title: elRepo.io
subtitle: bring down the cloud!
layout: page
hero_height: is-fullwidth is-fullheight
hero_image: /img/hero.elrepoio.png
hero_container: has-text-centered
hero_link_text: code
hero_link: http://gitlab.com/elRepo.io
---

# What would be the optimal system for culture sharing?
We consider the existing systems are sub-optimal, not because of techological barriers, but mainly due to powerful interests that create artificial scarcity to enable monetization of “cultural goods”. All digitizable culture could now be available to everyone, everywhere, all the time, with zero marginal cost. At the same time, the decisions around what content is relevant (and even findable) in mainstream information silos are a way of social control and public opinion orientation over which communities have no say.

The impact on humanity of letting profit based and concentrated interests steer the direction in which human culture evolves by limiting what information we can access and share, when and where, is a risk too big to keep taking. Almost any other big issue in our global society is directly related to this one.

# We are developing a decentralized repository of culture…
That allows the publishing and sharing of content in an organized way, suitable for audio, video, text and other formats. The system is built on top of mature technologies that leverage the capacities of distributed Community Networks. It will allow communities to share culture in a lasting way, priorizing local exchange without losing the ability to reach a global audience.

# Organized, resilient and with local perspective.
Communities will be able to:
- Curate, organize and classify content while keeping both data and metadata descentralized.
- Store content locally and reliably, reducing the risk of being censored or taken down.
- Reduce internet bandwidth needs by exchanging information locally even when internet connectivity is down.

# What are we working on?

This project has been initially funded by the Association for Progressive Communications (APC), of which AlterMundi is a member organization. The first development cycle ended on December 31, 2018. The work done during this year has helped us identify a suitable stack of technologies to use as a base for this project, work on their adaptation to our needs and develop an initial prototype.

We have worked on libretroshare (the base component of the stack) in multiple areas such as: improving Android support; creating a crossplatform service and distribute it as a standalone package that many applications can rely on; programming libretroshare automatic JSON API generator to expose libretroshare full potential to mobile and web based applications developers; improve IPv6 support; implement metadata indexing through libxapian and use it to give better results in distributed content and metadata search.

All our modifications to libretroshare have been welcomed upstream and will be part of libretroshare’s next offical release v0.6.5. We have also set up a redundant Tier1 infrastructure which helps create a seamless user experience for newcomers and facilitates progressive learning of how a F2F network behaves. The Tier1 infrastructure is important especially at the beginning when a critical mass has not yet formed; once more users join and the network begins resembling a small-world, the network effect is triggered and Tier1 infrastructure gradually loses its importance.

We are also developing a mobile and desktop UI which will let communities begin using the system and report their feedback to the iterative development process. Clients auto-discover neighbors in their local broadcast domain, facilitating communication within a community network or other networks that map their network stack to the existing social links. This, together with libretroshare’s offline capabilities, allows users to discover new friends, interact and share content without the need for internet access. All of the above will be the deliverables from our first development cycle.

![](/img/elrepoio_capturas_web2.png)

<section class="section">
  <div class="container">
	   <div class="columns is-multiline">
		<div class="column is-one-quater"><img src="/img/cropped-portada.png" alt=""></div>
		<div class="column is-one-quater"><img src="/img/cropped-portada-1-1.png" alt=""></div>
		<div class="column is-one-quater"><img src="/img/cropped-portada-1-2.png" alt=""></div>
		<div class="column is-one-quater"><img src="/img/cropped-portada-1-3.png" alt=""></div>
	</div>
  </div>
</section>


# Who we are?
AlterMundi, together with partnering organizations from every continent, has developed over the years a technology stack for communities that wish to create their own communications infrastructure. 

We believe that the soil we have cared for over the years building infrastructure technologies must serve as a nurturing ground for an ecosystem of services and applications that put the community in the center.

elRepo.io, our descentralized repository of culture, can become a cornerstone project in this ecosystem for community networks to create human centered local value.

<section class="section">
  <div class="container">
	   <div class="columns is-multiline">
		<div class="column is-one-third"> {% include card.html card_link="https://altermundi.net" card_image="/img/AlterMundi.png" card_title="AlterMundi" card_subtitle="... la pata tecnológica de ese Otro Mundo posible" card_content="AlterMundi fosters the emergence of a new paradigm based on freedom gained through peer to peer collaboration." %} </div>
        <div class="column is-one-third"> {% include card.html card_link="https://libremesh.org" card_image="/img/libremesh.png" card_title="LibreMesh" card_content="Is a modular framework for creating OpenWrt/LEDE-based firmwares for wireless mesh nodes. Several communities around the world use LibreMesh as the foundation of their local mesh firmwares." %} </div>
        <div class="column is-one-third"> {% include card.html card_link="https://librerouter.org" card_image="/img/librerouter.png" card_title="LibreRouter" card_content="The Libre Router project  has designed and produced a high performance multi-radio wireless router targeted at Community Networks needs. Global South reality and that of Latin America in particular has been specially considered in terms of cost and legal viability." %} </div>
	</div>
  </div>
</section>
